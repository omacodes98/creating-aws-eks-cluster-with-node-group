# Demo Project - AWS EKS cluster with a Node Group 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Configure necessary IAM Roles

* Create VPC with Cloudformation Template for Worker Nodes

* Create EKS cluster (Control Plane Nodes)

* Create Node Group for Worker Nodes and attach to EKS cluste

* Configure Auto-Scaling of worker nodes

* Deploy a sample application to EKS cluster

## Technologies Uesed 

* Kubernetes 

* AWS

* EKS 

## Steps 

Step 1: Create role for EKS service 

[Creating EKS role 1](/images/01_creating_role_for_EKS.png)

[Creating EKS role 2](/images/01_creating_role_for_EKS_1.png)


Step 2: Attach amazon EKS cluster policy to the EKS cluster service role 

[Attached amazonEKSclusterpolicy](/images/02_policy_eks_needs.png)

[EKS role created](/images/03_eks_role_created.png)

Step 3: Create vpc with cloudformation template for worker nodes  

[Cloudformation template](/images/04_cloudformation_template_for_worker_nodes.png)

[Components in stack](/images/05_all_components_in_stack_being_created.png)

[Output](/images/06_output.png)

Step 4: Create EKS cluster 

Step 5: Configure cluster by giving it appropriate name, kubernetes version and by assigning it to the eks cluster role that was created earlier

[Configure cluster](/images/07_configure_cluster.png)

Step 6: Specify networking by attaching VPC,subnets and security groups that were created by cloudformation template 

[Specify Networking](/images/08_specify_networking.png) 

Step 7: In the specify network section give cluster public and private endpoint 

[Specify Networking 1](/images/08_specify_networking_1.png)

[Specify Networking 2](/images/08_specify_networking_2.png)

[Configure logging](/images/09_configure_logging.png)

Step 8: Create kube config file, this file is used to allow you have access to eks cluster on your system

    aws eks update-kubeconfig --name eks-cluster-test 

[Create Kubeconfile](/images/10_creating_kube_config_file.png)

Step 9: Check if your system is connected to the EKS cluster. You can do this by checking the name of the nodes 

    kubectl get nodes 

[Connected to EKS cluster](/images/11_connected_to_eks.png)

Step 10: Create a role for ec2 which will be representing each individual worker nodes 

[Create role for ec2](/images/12_creating_role_for_ec2_which_will_be_representing_each_individual_worker_nodes.png)

Step 11: Attach the following policies to the role 

AmazonEKS_CNI_POLICY: to allow pods on different servers inside the cluster communicate with eachother 

AmazonEC2ContainerRegistryReadOnly

AmazonEKSWorkerNodePolicy

[Attaching cni policy](/images/13_attaching_cni_policy_so_that_pods_on_different_servers_inside_the_cluster_can_communicate_with_eachother.png)

[Attaching ECR policy](/imaegs/14_attaching_ec2_container_registry_policy.png)

[Attaching eks worker node policy](/images/15_attaching_eks_worker_node_policy.png)

Step 12: name role 

[Name Role](/images/16_name_role.png)

[Role](/Images/17_role_created.png)

Step 13: Create node group 

Step 14: While creating node group in the configure node group section give your node group a good name and then attach iam role created for node group which was done earlier 

[Configure node group](/images/18_creating_node_group_configuration_attach_policy_created_for_node_group.png)

Step 15: When creating node group in the set compute and scaling configuration set ami type, capacity type, instance type and disk size to the appropriate option. This will depends on things like the type of application you want to deploy to the eks cluster.

[Compute and scaling configuration](/images/19_node_group_compute_configuration_this_is_config_for_ec2_instance.png)

Step 16: In node group scaling configure the amount of worker ndoes you want running in your cluster

[Group scaling config](/images/20_Node_group_scaling_configuration_the_amount_of_worker_nodes_you_want_running_in_your_cluster.png)

Step 17: In the network configuration you give it appropriate subnets which was created by the cloudformation and ssh key to be able to have access the the ec2 instances for things like configuration or identifying necessary errors.

[Network configuration](/images/21_network_configuration.png)

Step 18: Check if everything is running fine 

[ec2 instances running](/images/22_ec2_instances_running.png)

[node group on aws](/images/23_node_group_active_on_aws.png)

[node on terminal](/images/24_nodes_on_terminal.png)

Step 19: Create policy to give node group for autoscaling 

[Creating policy for node group autcaling](/images/25_creating_policy_to_give_node_group_for_autoscaling_to_work.png)

[Created policy autoscaling](/images/26_policy_created_for_autoscaling.png)

Step 20: Attach policy to nodegrouprole 

[attaching autoscaling policy to node group role](/images/27_attaching_policies_to_node_group_role.png)

Step 21: Deploy autoscaler 

    kubectl apply -f https://raw.githubsercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/example/cluster-autoscaler-autodiscover.yaml 

[Deploy autoscaler](/images/28_deploying_autoscaler.png)

Step 22: edit the autoscaler deployment config by adding annotation

[Adding annotation](/images/29_edit_autoscaler_deployment_config_by_adding_annotation.png)

Step 23: Add cluster name to deployment configuration

[Adding cluster name](/images/30_add_cluster_name_to_deployment_configuration.png)

Step 24: Add this options --balance-similar-node-groups, --skip-nodes-with-system-pods=false in command in deployment configuration

[Adding options](/images/31_adding_some_options_in_the_command_in_deployment_configuration.png)

Step 25: Change image version in deployment configuration 

[Finding autoscaler image version](/images/32_finding_autoscaler_image_version_that_matches_kubernetes_version.png)

[changed image](/images/33_changed_image_version_in_deployment_configuration.png)

[Updated deployment configuration](/images/34_updated_deployment_configuration.png)

Step 26: Deploy nginx with service on eks 

    kubectl apply -f nginx-config.yaml

[Deploying nginx](/images/35_deploying_nginx_with_service_on_eks.png)

Step 27: Check application on browser 

[Application on browser](/images/37_application_on_browser.png)

[loadbalancer](/images/36_loadbalancer_that_was-automatically_created.png)


Note: you dont have to worry about installing
Container run time
Kubelet (kubernetes worker process)
Kube-proxy (Kubernetes worker process)

in the nodes this is because this is installed on the ec2 servers when we create them through node group,which is good because you dont have to worry about installing them yourself

## Usage 

    kubectl apply -f nginx-config.yaml

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/creating-aws-eks-cluster-with-node-group.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/creating-aws-eks-cluster-with-node-group

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.